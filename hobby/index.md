---
layout: default
---

# Mini Attributes


## Core Attributes

Each Mini has a set of attributes that represent its effectiveness in battle. These attributes are not directly affected by [Equipment](/equipment) choices; they represent the Mini's natural abilities without armed equipment.

**Strength (`STR`)** represents both how naturally resistant to damage the Mini is, and how physically damaging its melee attacks are. It is determined by the Mini's chosen yarn weight and its size.

**Speed (`SPD`)** is how fast the Mini is, equal to the number of inches the Mini can move every turn. This value is inversely proportional to the Mini's `STR`, where `SPD = 10 - STR`.

**Accuracy (`ACC`)** is equal to the number of eyes the Mini has, and affects combat effectiveness.

**Wounds (`WND`)** are the number of total wounds that the Mini can sustain before considered dead. `WND` is determined by the Mini's yarn weight.


## Derived Attributes

Some attributes are situationally derived, which typically means they depend on an equipped item or are decided by a choice of action.

**Melee Attack (`MAT`)** represents the relative effectiveness and potential damage of a Mini's physical attack, either unarmed or with a melee weapon. It is calculated by adding the Mini's `STR` and `ACC` with the chosen weapon's `POW`. An unarmed attack has `0 POW`.

**Ranged Attack (`RAT`)** represents the relative effectiveness and potential damage of a Mini's attack from a distance with a certain weapon. It is calculated by adding the Mini's `ACC` with the weapon's `POW`.

**Defense (`DEF`)** represents the relative effectiveness of the Mini's defensive actions in combat. It is calculated by adding the Mini's `STR` and `ACC` with the sum `ARM` of any equipped armor.


## Yarn Types

| Yarn        | Strength | Wounds  | Cost   |
|:----------- |:-------- |:------- |:------ |
| Light       | `1 STR`  | `1 WND` | `1 pt` |
| Medium      | `2 STR`  | `2 WND` | `2 pt` |
| Bulky       | `3 STR`  | `3 WND` | `3 pt` |
| Super Bulky | `4 STR`  | `4 WND` | `4 pt` |


## Mini Sizes

| Size   | Diameter   | Strength | Body Parts | Cost   |
|:------ |:--------   |:-------- |:---------- |:------ |
| Small  | 1-2&Prime; | `1 STR`  | 2          | `2 pt` |
| Normal | 2-3&Prime; | `2 STR`  | 4          | `4 pt` |
| Large  | 3-4&Prime; | `3 STR`  | 6          | `6 pt` |


## Preparation

Before you begin crafting a Mini, you should be aware of the various choices and their implications. The two biggest decisions to consider at this point are choosing the yarn weight, and how large to make the Mini. These two things are not changeable once the Mini is created.

You also might want to consider placing eyes on the Mini before closing the bottom, as it might be easier to attach them at this point than later on. Most other body parts can be attached easily after the body is finished.


## Mini Pattern

The crochet pattern for a basic Mini body is very straight-forward.

1. 8 *sc* into a [magic ring](http://www.craftsy.com/blog/2013/09/demystifying-the-magic-ring/)
2. Increase in each loop pair (2 into each); 8 times
3. 1 *sc*, then increase; 8 times
4. 2 *sc*, then increase; 8 times
5. 3 *sc*, then increase; 8 times

At this point, start measuring the diameter of the crocheted piece. If the diameter is more than &frac14;&Prime; away from the desired base diameter size, keep incrementing the number of *sc* before increases and measure each round. So potentially, you'd have &ldquo;Round 6: 4 *sc*, then increase; 8 times&rdquo;, &ldquo;Round 7: 5 *sc*, then increase; 8 times&rdquo;, etc.

When the diameter is within &frac14;&Prime; of the desired diameter, you should stop increasing, and simply *sc* into each loop pair in rounds, until the Mini body is as tall as you want. When the body is the desired size, cut the yarn a few inches from the body, slip stitch one, and weave the loose end into a few loops to secure it in place. You can then stuff the Mini body as you want; it's recommended to over-stuff it slightly.

To create the bottom of the Mini, crochet the same as above, following the steps until you decided to stop increasing. At that point, you're done with the bottom. Cut the yarn about 6&Prime; from the end, slip stitch, then weave the bottom and body together with the yarn, using an embroidery needle. Once the bottom is secure, weave any remaining loose thread into the body and cut off any extra.


## Eyes

Every Mini is required to have at least one eye. Each eye added to the Mini costs the number of points equal to the number eye it is (1 pt for first eye, 2 pt for second eye, etc.).


## Body Parts

| Body Part | Multiple? | Gives | Effect | Cost |
|:--------- |:--------- |:----- |:------ |:---- |
| Arm | Yes | [Punch](/equipment#abilities) | - | `2 pt` |
| Feet | No | [Kick](/equipment#abilities), [Run](/equipment#abilities) | `+1 SPD` | `2 pt` |
| Hair | No | 2 [Spells](/equipment#spells) | Upgrade to "Weaver" | `5 pt` |
| Mouth | No | [Bite](/equipment#abilities), [Yell](/equipment#abilities) | - | `2 pt` |
