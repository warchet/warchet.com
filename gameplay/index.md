---
layout: default
---

# Overview

The number of players allowed in a game of Warchet depends on the battlefield itself. There can be as many players as there are *deployment zones*. A *deployment zone* is used to determine the starting placement of the players' armies;; each player will be assigned one at the initial setup. Typically, these are table edges, but can be anything agreed upon by the players.

The player whose turn ends with no opponent Minis on the battlefield wins the battle. Any player may concede a game, and their Minis are removed at the end of the turn they concede on.


# Setup & Deployment

Players decide on a maximum number of points for a game, and each player assembles an army worth up to that number of points.

Players roll to determine turn order. For each player's deployment turn, the player chooses an unassigned deployment zone. They then place all their Minis within 12&Prime; of this zone. Minis may not be moved during another player's deployment.


# Turns

Each Mini can do two things each turn: make a movement, and take an action. They can be done in either order, but you cannot move, take an action, and move again.


## Movement

Each Mini can move any number of inches up to their `SPD` attribute, and must make all movement at one time.


## Actions

Each Mini can take one action, determined by what equipment they're armed with, and what abilities they have. If a Mini is within 1&Prime; of an enemy, it can melee attack that enemy.


# Combat


## Derived Combat Values

* `MAT = STR + ACC + POW`
* `RAT = ACC + POW`
* `DEF = STR + ACC + ARM`


## Combat Rolls

For direct damage attacks, the attacker's Attack value (`MAT` for melee, `RAT` for ranged) is compared to the defender's `DEF`, determining the required roll on a `d10` to cause a Wound. If the Attack value and `DEF` are equal, a *6+* is required. For each point higher the Attack is than the `DEF`, a lower number roll is required, to a minimum of *2+* (a roll of 1 always fails).

Similarly, for each point higher the `DEF` is than the Attack, a higher number roll is required. There is no maximum roll, so if the `DEF` is 5 higher than the Attack, the attack will not have any chance of dealing damage.
