---
layout: default
---

# Weapons

Each weapon requires a certain level of `STR` to be carried, and a certain number of arms in order to be used. Note that a weapon does not need to be held at all times, which means that a single Mini can be armed with multiple weapons, and can choose which weapon to use each combat phase if they have the required number of arms to do so.


## Weapon Attributes

**Attacks (`ATK`)** are the number of attacks a weapon makes per combat phase. This means that `ATK` number of dice are rolled when attacking with this weapon.

**Power (`POW`)** represents the relative damaging potential of the weapon.

**Range (`RNG`)** is the maximum number of inches that a ranged weapon can effectively target an enemy.


## Melee Weapons

| Weapon | Req. `STR` | Req. # Arms | Attacks | Power | Cost |
|:------ |:---------- |:----------- |:------- |:----- |:---- |
| Blunt Object | - | - | `1 ATK` | `1 POW` | - |
| Dagger | `2 STR` | 1 arm | `1 ATK` | `2 POW` | `1 pt` |
| Dual Daggers | `3 STR` | 2 arms | `2 ATK` | `2 POW` | `2 pt` |
| Sword (One-Handed) | `4 STR` | 1 arm | `1 ATK` | `3 POW` | `3 pt` |
| Sword (Two-Handed) | `5 STR` | 2 arms | `1 ATK` | `5 POW` | `5 pt` |


## Ranged Weapons

| Weapon | Req. `STR` | Req. # Arms | Range | Attacks | Power | Cost |
|:------ |:---------- |:----------- |:----- |:------- |:----- |:---- |
| Bow | `4 STR` | 2 arms | `18" RNG` | `1 ATK` | `4 POW` | `4 pt` |
| Rifle | `2 STR` | 2 arms | `18" RNG` | `1 ATK` | `5 POW` | `5 pt` |
| Spear | `3 STR` | 1 arm | `10" RNG` | `1 ATK` | `3 POW` | `3 pt` |


# Equipment


## Worn Items

| Item | Req. `STR` | Armor | Cost |
|:---- |:---------- |:----- |:---- |
| Hat | `1 STR` | `2 ARM` | `1 pt` |
| Helmet | `3 STR` | `4 ARM` | `3 pt` |
| Scarf | `1 STR` | `1 ARM` | `1 pt` |
| Shield | `4 STR` | `5 ARM` | `5 pt` |


## Carried Items

| Item | Details | Cost |
|:---- |:------- |:---- |
| Cup of Tea | *Take a Sip* to receive `+3 STR` next turn | `3 pt` |
| Grenade | Single use; *Throw* to explode in 3&Prime; radius, `1 ATK, 5 POW` hit | `1 pt` |
| Musical Instrument | *Play* to grant friendly Minis within 5&Prime; a `+1 ATK` bonus this turn | `4 pt` |
| Plate of Cookies | Friendly Minis within 3&Prime; receive a `+1 DEF` bonus | `4 pt` |
| Yarn Kit | *Use* on a friendly Mini within 3&Prime; to remove one Wound | `3 pt` |


# Abilities

| Ability | Range | Given&nbsp;By | Effect |
|:------- |:----- |:-------- |:------ |
| Bite | `1" RNG` | [Mouth](/hobby#body-parts) | Special melee attack, wounds on `(target's DEF)+` |
| Drop&nbsp;Item | `2" RNG` | - | Unequips an equipped item and places it onto the ground within 2&Prime; of the Mini |
| Kick | `1" RNG` | [Feet](/hobby#body-parts) | Knocks target enemy 3&Prime; straight away from Mini |
| Pick&nbsp;Up&nbsp;Item | `2" RNG` | - | Picks up and equips item from ground, gaining abilities and attributes given by item |
| Punch | `1" RNG` | [Arm](/hobby#body-parts) | Melee attack with `(# arms) ATK, (lowest yarn weight of arms) POW` |
| Run | - | [Feet](/hobby#body-parts) | Move an extra `⌊d10 / 2⌋"`<br>*Minis with Super Bulky yarn cannot run* |
| Throw | `8" RNG` | - | Throws an equipped item to the chosen target spot, possibly [scattering](#scattering); if the throw item has an effect, it is triggered when it lands |
| Yell | `4" RNG` | [Mouth](/hobby#body-parts) | Target Mini suffers `-1 DEF` until end of turn |


## Scattering

Whenever an action is taken that involves targeting a location, the actual destination has a possibility of scattering. Once the target location is chosen (however that takes place for the current action), then 2 `d10` are rolled. The Mini's `ACC` is subtracted from the *lower result*, and if the result is greater than 0, the target spot is moved that many inches. The direction of the deviation can be determined by any method. Once the final target spot is determined, the effect of the action occurs.


# Spells

| Spell | Range | Effect | Cost |
|:----- |:----- |:------ |:---- |
| Blind | `8" RNG` | Target Mini suffers `-1 ACC` until end of turn | `4 pt` |
| Haste | `8" RNG` | Target Mini has double `SPD` until end of turn | `8 pt` |
| Heal | `8" RNG` | Target Mini removes a Wound | `6 pt` |
| Refresh | `8" RNG` | Target Mini gains an extra action this phase | `6 pt` |
| Sleep | `8" RNG` | Target Mini cannot move or take an action until the end of the opponent's next turn | `8 pt` |
| Teleport | `12" RNG` | Places Weaver at a scattered target position; Weaver cannot cast a spell next turn | `10 pt` |


## Casting Spells

A Mini may cast a spell on a target if the target is within the spell's `RNG`. A single `d10` is rolled to cast a spell, with a successful result of `(8 - ACC)+`. If the roll result passes, the spell takes effect immediately. *A roll of 1 always fails.*
