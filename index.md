---
layout: default
---

## What is this?

Warchet is a miniature wargame played with handmade crochet figures. It can be played between any number of people on a tabletop, using die and opposing miniature armies.

## Isn't crocheting for wrinkled old women?

Yes, but we're crocheting soft cuddly killing machines.

## Sweet. How do I get started?

You should learn about crocheting itself first, then head to the [Hobby Page](/hobby).
